import pstats
import cv2
import argparse
import numpy as np
import timeit
import cProfile
import glob
from tqdm import tqdm
import time
import itertools
from operator import itemgetter

def find_contours(binary_image: any, original_frame: any, threshold_contours: int):
    """

    :param binary_image:
    :param original_frame:
    :return:
    """
    #This is used to obtain the contours in the image
    contours, hierarchy = cv2.findContours(binary_image, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_SIMPLE)
    #Gonna draw the contours now onto the image.
    image_copy = original_frame.copy()
    # cv2.drawContours(image_copy, contours=contours, contourIdx=-1, color=(0,255,255),thickness=2)
    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        # print(cv2.contourArea(contour))
        if cv2.contourArea(contour) > threshold_contours:
            cv2.rectangle(image_copy,(x,y), (x+w, y+h), (255,255,0))
    return image_copy

def extract_background_static(camera_frame: any, background_frame: any):
    """
    This is the main processing of this program, it is used to subtract the background and return a binary image.
    :param camera_frame:
    :param background_frame:
    :return:
    """
    kernel = np.ones((3, 3), np.uint8)

    final_subtraction = cv2.absdiff(camera_frame,background_frame)
    final_subtraction[abs(final_subtraction) < 30.0] = 0
    binary_final = cv2.cvtColor(final_subtraction, cv2.COLOR_BGR2GRAY)
    binary_final = cv2.erode(binary_final, kernel, iterations=2)
    binary_final = cv2.dilate(binary_final, kernel, iterations=1)
    binary_final[binary_final > 5] = 255
    binary_final[binary_final < 5] = 0

    return binary_final

def calculate_x_histogram(frame: any, shape:[]):
    """
    This calculates the density of the x pixels
    :param frame:
    :param shape:
    :return:
    """
    density_array = []
    # Iterate through the x pixel bins and sum all the 1s.
    for i in range(int(shape[0])):
        white_pixels = np.count_nonzero(frame[:,i])
        density_array.append(white_pixels)
    return density_array


def calculate_y_histogram(frame: any, shape:[], ranges_x: []):
    """

    :param frame:
    :param shape:
    :param density_x:
    :return:
    """
    density_array = []

    for i in range(int(shape[1])):
        white_pixels = np.count_nonzero(frame[i,:])
        density_array.append(white_pixels)

    y_array = np.array(density_array)
    indices_y = np.argwhere(y_array > 50)
    return indices_y

    # Iterate through the x pixel bins and sum all the 1s.
    # for i in range(int(shape[1])):
    #     white_pixels = np.count_nonzero(frame[i,indices_x[0][0]:indices_x[len(indices_x)-1][0]])
    #     density_array.append(white_pixels)
    # return density_array

def create_bounding_box_histogram(processed_frame: any, density_x: any):
    """

    :param processed_frame:
    :param density_x:
    :return:
    """
    boxes = []
    x_array = np.array(density_x)
    indices_x = np.argwhere(x_array > 50)
    ranges_x = []
    for k,g in itertools.groupby(enumerate(indices_x), lambda x:x[0]-x[1]):
        group = (map(itemgetter(1),g))
        group = list(map(int,group))
        ranges_x.append((group[0],group[-1]))

    indices_y = np.array(calculate_y_histogram(processed_frame, [1280, 720], ranges_x))

    if len(indices_y) and len(indices_x):
        for box_coordinates in ranges_x:
            box = []
            box.append(box_coordinates[0])
            box.append(indices_y[0][0])
            box.append(box_coordinates[1] - box_coordinates[0])
            box.append(indices_y[len(indices_y)-1][0] - indices_y[0][0])
            boxes.append(box)

    return boxes


def draw_bounding_boxes_histogram(frame:any, boxes: []):
    image_copy = frame.copy()
    for box in boxes:
        cv2.rectangle(image_copy, (box[0], box[1]), (box[0] + box[2], box[1] + box[3]), (255, 255, 0))

    return image_copy

def background_extract_video(path: str, single_video: int, save_path: str, show_video: int, save_videos: int):
    """
    This is the main openCV core, opening the videos and sending it to the appropriate functions for processing.
    :param path: The folder path containing the data.
    :param single_video: This is an into to choose between a single video or multiple processing.
    :param save_path: This is the path to save the processed videos to.
    :param show_video: This is a check to see if openCV show display the video or not.
    :param save_videos: This is a check to save the videos or not.
    :return:
    """
    # 0 means video folder, 1 is a single vide

    if single_video == 0:
        video_number = 1
        counter_first_frame = 0
        for video_file in tqdm(glob.glob(path)):
            video_capture = cv2.VideoCapture(video_file)
            counter_first_frame = 0
            # all_hist_x = []

            #Get the width and height of original video for the save file
            if save_videos == 1:
                width_frame = int(video_capture.get(3))
                height_frame = int(video_capture.get(4))
                save_video_path = save_path + str(video_number) + ".mp4"
                save_video_final = cv2.VideoWriter(save_video_path, cv2.VideoWriter_fourcc(*'MP42'), 30.0, (width_frame, height_frame))
                video_number += 1

            #do the processing on the video
            pTime = 0
            while video_capture.isOpened():
                ret, frame = video_capture.read()
                # Test resizing the image
                cTime = time.time()
                fps = 1/ (cTime - pTime)
                pTime = cTime
                if ret:
                    # frame = cv2.resize(frame, (640, 480), fx=0, fy=0, interpolation=cv2.INTER_CUBIC)
                    if counter_first_frame == 0:
                        background_static_frame = frame
                        counter_first_frame += 1


                    processed_frame = extract_background_static(frame, background_static_frame)
                    histogram_data_x = calculate_x_histogram(processed_frame, [video_capture.get(3), video_capture.get(4)])
                    # all_hist_x.append(histogram_data_x)
                    boxes = create_bounding_box_histogram(processed_frame, histogram_data_x)
                    final_histogram = draw_bounding_boxes_histogram(frame, boxes)
                    # final_contours = find_contours(processed_frame, frame, 2000)
                    cv2.putText(final_histogram, str(int(fps)), (70, 50), cv2.FONT_HERSHEY_PLAIN, 3, (255, 0, 255), 3)
                    if save_videos == 1:
                        save_video_final.write(final_histogram)

                    if show_video == 1:
                        cv2.imshow("Processed Frame", processed_frame)
                        cv2.imshow("Original Frame", frame)
                        cv2.imshow("Contours",final_histogram)

                    if cv2.waitKey(25) & 0xFF == ord('q'):
                        break
                else:
                    break
            # all_hist_x_np = np.array(all_hist_x)
            # myData = pd.DataFrame(all_hist_x_np)
            # myData.to_csv(r"data1.csv")


    video_capture.release()
    if save_videos == 1:
        save_video_final.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    video_folder_path = "outputDataTestVideo/*.*"
    save_path_folder = "TestingFolder/ProcessedCowStatic"
    cProfile.run('background_extract_video(video_folder_path, 0, save_path_folder, 1,0)', "output_extract_cow_static.dat")

    with open("output_time1.txt", "w") as f:
        p = pstats.Stats("output_extract_cow_static.dat", stream=f)
        p.sort_stats("time").print_stats()

    with open("output_calls1.txt", "w") as f:
        p = pstats.Stats("output_extract_cow_static.dat", stream=f)
        p.sort_stats("calls").print_stats()