import cv2
import argparse
import numpy as np
import timeit
import cProfile
import glob
from tqdm import tqdm
import time
threshold = 0.2 # 60 percent confident
nmsThresh = 0.3
classNames = []

#This code is used to detect the cow and extrapolote the image, this will then be downsampled and be prepared for
# feature extraction using simialr AI methods.
def findCow(output, frame):
    heightTarget, widthTarget, channelTarget = frame.shape
    boundingBox = [] #Value of x, y width and height
    classIds = []
    confidences = []

    for out in output:
        for det in out:
            scores = det[5:]
            classId = np.argmax(scores)
            confidence = scores[classId]
            if confidence > threshold:
                w,h = int(det[2] * widthTarget), int(det[3] * heightTarget)
                x, y = int(det[0] * widthTarget - (w/2)), int(det[1] * heightTarget - (h/2))
                boundingBox.append([x,y,w,h])
                classIds.append(classId)
                confidences.append(float(confidence))

    # print(len(boundingBox), len(classIds),len(confidences))
    indices = cv2.dnn.NMSBoxes(boundingBox, confidences, threshold, nmsThresh)

    for i in indices:
        i = i[0]
        box = boundingBox[i]
        x,y,w,h = box[0], box[1], box[2], box[3]
        cv2.rectangle(frame,(x,y),(x+w,y+h),(255,255,0),2)
        cv2.putText(frame,f'{classNames[classIds[i]].upper()}{int(confidences[i]*100)}%',(x,y - 10), cv2.FONT_HERSHEY_SIMPLEX,0.6,(255,0,255),2)


with open('coco.names', 'rt') as f:
    classNames = f.read().rstrip('\n').split('\n')

#This is for glob to select the videos
VideoPath = 'C:/Users/steve/OneDrive/Documents/Work/Data/Videos/*.*'
vid_number = 1

modelConfig = 'yolov3.cfg'
modelWeights = 'yolov3.weights'

resolutionTarget = 320

network = cv2.dnn.readNetFromDarknet(modelConfig,modelWeights)
network.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
network.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
#####################################

#Every 20th frame it will run the findcow script
counterForFrames = 1
initCounter = 0
for filename in tqdm(glob.glob(VideoPath)):
    videoCapture = cv2.VideoCapture(filename)
    counter = 0

    width = int(videoCapture.get(3))
    height = int(videoCapture.get(4))
    # saveFile = "TestingProcessedCow" + str(vid_number) + ".mp4"

    # saveVid = cv2.VideoWriter(saveFile, cv2.VideoWriter_fourcc(*'MP42'), 30.0, (width, height))
    pTime = 0
    while videoCapture.isOpened():
        ret, frame = videoCapture.read()
        cTime = time.time()
        fps = 1/(cTime - pTime)
        pTime = cTime
        if ret:
            # get the first frame and show it
            if initCounter == counterForFrames:
                blob = cv2.dnn.blobFromImage(frame, 1/255, (resolutionTarget, resolutionTarget), [0,0,0], 1, crop=False)
                network.setInput(blob)

                #get the layer names
                layerNames = network.getLayerNames()
                outputLayerNames = [layerNames[i[0] - 1] for i in network.getUnconnectedOutLayers()]
                #only interested in the outputs of these layers. Thye have the prediction scores.
                outputs = network.forward(outputLayerNames)
                findCow(outputs,frame)
                initCounter = 0
            initCounter += 1
            # print(fps)
            cv2.putText(frame, str(int(fps)), (70, 50), cv2.FONT_HERSHEY_PLAIN, 3, (255, 0, 255), 3)
            # saveVid.write(frame)

            cv2.imshow("Original", frame )
            # cv2.imshow("Final", grayFinal)

            if cv2.waitKey(25) & 0xFF == ord('q'):
                break
        else:
            break

    vid_number += 1

videoCapture.release()
# saveVid.release()
cv2.destroyAllWindows()

