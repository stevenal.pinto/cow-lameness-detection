import cv2
import argparse
import numpy as np
import timeit
import cProfile
import glob
from tqdm import tqdm
import time
import pstats

def find_contours(binary_image: any, original_frame: any, threshold_contours: int):
    """
    This function is used to find the contours after the image has had the background rejected.
    :param binary_image:
    :param original_frame:
    :return:
    """
    #This is used to obtain the contours in the image
    contours, hierarchy = cv2.findContours(binary_image, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_SIMPLE)
    #Gonna draw the contours now onto the image.
    image_copy = original_frame.copy()
    # cv2.drawContours(image_copy, contours=contours, contourIdx=-1, color=(0,255,255),thickness=2)
    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        # print(cv2.contourArea(contour))
        if cv2.contourArea(contour) > threshold_contours:
            cv2.rectangle(image_copy,(x,y), (x+w, y+h), (255,255,0))
    return image_copy

def denoise_frame(frame:any, binary_thresh: int):
    """
    This function is used to reduce the amount of noise present in the image.
    :param frame:
    :return:
    """
    kernel = np.ones((3,3), np.uint8)
    frame = cv2.erode(frame, kernel, iterations=1)
    frame = cv2.dilate(frame, kernel, iterations=1)
    # Push to 1s and zeros
    frame[abs(frame) < 200] = 0
    return frame

def background_extract_video(path: str, single_video: int, save_path: str, show_video: int, save_video:int):
    """
    This will perform the background subtraction using the built in method for MOG (Mean of gaussian
    :param path: The folder with the data videos
    :param single_video: The check if a single video shoudl be processed or a folder of data
    :param save_path: The path that the processed videos need to be saved to.
    :param show_video: An index to check if the video should be shwon or not.
    :return:
    """
    if single_video == 0:
        background_subtract = cv2.createBackgroundSubtractorMOG2()

        video_number = 1
        for video_file in tqdm(glob.glob(path)):
            video_capture = cv2.VideoCapture(video_file)

            #Get the width and height of original video for the save file
            width_frame = int(video_capture.get(3))
            height_frame = int(video_capture.get(4))
            if save_video == 1:
                save_video_path = save_path + str(video_number) + ".mp4"
                save_video_final = cv2.VideoWriter(save_video_path, cv2.VideoWriter_fourcc(*'MP42'), 30.0, (width_frame, height_frame))
                video_number += 1

            #do the processing on the video
            pTime = 0
            while video_capture.isOpened():
                ret, frame = video_capture.read()
                cTime = time.time()
                fps = 1 / (cTime - pTime)
                pTime = cTime
                if ret:
                    # frame = cv2.resize(frame, (640, 480), fx=0, fy=0, interpolation=cv2.INTER_CUBIC)
                    processed_frame = background_subtract.apply(frame)
                    processed_frame = denoise_frame(processed_frame, 120)
                    #get and annotate the contours onto the image
                    final_video = find_contours(processed_frame, frame, 2000)

                    cv2.putText(final_video, str(int(fps)),(70,50), cv2.FONT_HERSHEY_PLAIN, 3, (255,0,255), 3)
                    if save_video == 1:
                        save_video_final.write(final_video)
                    if show_video == 1:
                        cv2.imshow("Processed Frame", processed_frame)
                        cv2.imshow("Original Frame", frame)
                        cv2.imshow("contours", final_video)

                    if cv2.waitKey(25) & 0xFF == ord('q'):
                        break
                else:
                    break

    video_capture.release()
    if save_video == 1:
        save_video_final.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    video_paths = "outputDataTestVideo/*.*"
    save_path = "TestingFolder/ProcessedBuiltInCowMOG"
    cProfile.run('background_extract_video(video_paths, 0, save_path, 1,0)',"output_extract_cow_MOG.dat")

    with open("output_time3.txt", "w") as f:
        p = pstats.Stats("output_extract_cow_MOG.dat", stream=f)
        p.sort_stats("time").print_stats()

    with open("output_calls3.txt", "w") as f:
        p = pstats.Stats("output_extract_cow_MOG.dat", stream=f)
        p.sort_stats("calls").print_stats()