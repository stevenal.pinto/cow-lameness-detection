import cv2
import numpy as np
import glob
import tqdm

def findCow(output, frame):
    heightTarget, widthTarget, channelTarget = frame.shape
    boundingBox = [] #Value of x, y width and height
    classIds = []
    confidences = []

    for out in output:
        for det in out:
            scores = det[5:]
            classId = np.argmax(scores)
            confidence = scores[classId]
            if confidence > threshold:
                w,h = int(det[2] * widthTarget), int(det[3] * heightTarget)
                x, y = int(det[0] * widthTarget - (w/2)), int(det[1] * heightTarget - (h/2))
                boundingBox.append([x,y,w,h])
                classIds.append(classId)
                confidences.append(float(confidence))

    indices = cv2.dnn.NMSBoxes(boundingBox, confidences, threshold, nmsThresh)
    for i in indices:
        box = boundingBox[i[0]]
        x, y, w, h = box[0], box[1], box[2], box[3]
        cv2.rectangle(frame,(x,y),(x+w,y+h),(255,255,0),2)

        cv2.putText(frame,f'{classNames[classIds[i[0]]].upper()}{int(confidences[i[0]]*100)}%',(x,y - 10), cv2.FONT_HERSHEY_SIMPLEX,0.6,(255,0,255),2)


def YOLO():
    video_number = 1
    for video_file in glob.glob(video_folder_path):
        # print(video_file)

        video_cap = cv2.VideoCapture(video_file)
        width_frame = int(video_cap.get(3))
        height_frame = int(video_cap.get(4))
        save_video_path = save_path + str(video_number) + ".mp4"
        save_video_final = cv2.VideoWriter(save_video_path, cv2.VideoWriter_fourcc(*'MP42'), 30.0,
                                           (width_frame, height_frame))
        video_number += 1
        while video_cap.isOpened():
            ret, frame = video_cap.read()
            if ret:
                blob = cv2.dnn.blobFromImage(frame, 1 / 255, (resolutionTarget, resolutionTarget), [0, 0, 0], 1,
                                             crop=False)
                network.setInput(blob)
                layerNames = network.getLayerNames()
                # print(layerNames)
                # print(network.getUnconnectedOutLayers())
                outputLayerNames = [layerNames[i[0] - 1] for i in network.getUnconnectedOutLayers()]
                # only interested in the outputs of these layers. Thye have the prediction scores.
                outputs = network.forward(outputLayerNames)
                findCow(outputs, frame)
                save_video_final.write(frame)
                # cv2.imshow("Original frame", frame)
                if cv2.waitKey(25) & 0xFF == ord('q'):
                    break
            else:
                break


def video_capture_live():
    video_cap = cv2.VideoCapture("rtsp://admin:IZADLX@192.168.1.66:554:H.264")

    while video_cap.isOpened():
        ret, frame = video_cap.read()
        if ret:
            blob = cv2.dnn.blobFromImage(frame, 1 / 255, (resolutionTarget, resolutionTarget), [0, 0, 0], 1,
                                         crop=False)
            network.setInput(blob)
            layerNames = network.getLayerNames()
            # print(layerNames)
            # print(network.getUnconnectedOutLayers())
            outputLayerNames = [layerNames[i[0] - 1] for i in network.getUnconnectedOutLayers()]
            # only interested in the outputs of these layers. Thye have the prediction scores.
            outputs = network.forward(outputLayerNames)
            findCow(outputs, frame)
            cv2.imshow("Original frame", frame)
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break
        else:
            break

if __name__ == "__main__":
    threshold = 0.2  # 60 percent confident
    nmsThresh = 0.3
    classNames = []

    with open('obj.name', 'rt') as f:
        classNames = f.read().rstrip('\n').split('\n')

    video_folder_path = 'C:/Users/user pc/Documents/Work/Honours/Data/ProperTesting/*'
    save_path = 'C:/Users/user pc/Documents/Work/Honours/Data/YOLOSaveData/ProceessedYOLO'
    modelConfig = 'yolov3_custom.cfg'
    modelWeights = 'yolov3_custom_2000.weights'
    resolutionTarget = 416

    network = cv2.dnn.readNetFromDarknet(modelConfig, modelWeights)
    network.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
    network.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
    YOLO()
