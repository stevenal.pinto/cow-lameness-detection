import cv2
import glob
import numpy as np
import os
from tqdm import tqdm

def extract_frames(folder_path:str , save_path:str):
    """
    This will save frames from multiple videos
    :param folder_path:
    :param save_path:
    :return:
    """
    counter_naming = 0
    counter_checks = 0
    for video in tqdm(glob.glob(folder_path)):
        video_capture = cv2.VideoCapture(video)
        frame_save = 20
        while video_capture.isOpened():
            ret, frame = video_capture.read()
            if ret:
                if counter_checks == frame_save:
                    cv2.imwrite(save_path+str(counter_naming)+'.jpg', frame)
                    counter_naming+=1
                    counter_checks = 0
                else:
                    counter_checks+=1

                if cv2.waitKey(25) & 0xFF == ord('q'):
                    break
            else:
                break


if __name__ == "__main__":
    video_folder_path = "Testing_NN/*.*"
    save_path_folder = "Frames/frame"
    extract_frames(video_folder_path,save_path_folder)
