import pstats
import cv2
import argparse
import numpy as np
import timeit
import cProfile
import glob
from tqdm import tqdm
import time
import matplotlib.pyplot as plt

def find_contours(binary_image: any, original_frame: any, threshold_contours: int):
    """

    :param binary_image:
    :param original_frame:
    :return:
    """
    #COnvert to gr
    #This is used to obtain the contours in the image
    contours, hierarchy = cv2.findContours(binary_image, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_SIMPLE)
    #Gonna draw the contours now onto the image.
    image_copy = original_frame.copy()
    cv2.drawContours(image_copy, contours=contours, contourIdx=-1, color=(0,255,255),thickness=2)
    # for contour in contours:
    #     x, y, w, h = cv2.boundingRect(contour)
    #     # print(cv2.contourArea(contour))
    #     if cv2.contourArea(contour) > threshold_contours:
    #         cv2.rectangle(image_copy,(x,y), (x+w, y+h), (255,255,0))
    return image_copy

def extract_background_static(camera_frame: any, background_frame: any):
    """
    This is the main processing of this program, it is used to subtract the background and return a binary image.
    :param camera_frame:
    :param background_frame:
    :return:
    """
    kernel = np.ones((3, 3), np.uint8)

    final_subtraction = cv2.absdiff(camera_frame,background_frame)
    final_subtraction[abs(final_subtraction) < 30.0] = 0
    binary_final = cv2.cvtColor(final_subtraction, cv2.COLOR_BGR2GRAY)
    binary_final = cv2.erode(binary_final, kernel, iterations=2)
    binary_final = cv2.dilate(binary_final, kernel, iterations=1)
    binary_final[binary_final > 5] = 255
    binary_final[binary_final < 5] = 0

    return binary_final

def background_extract_video(path: str, single_video: int, save_path: str, show_video: int, save_videos: int):
    """
    This is the main openCV core, opening the videos and sending it to the appropriate functions for processing.
    :param path: The folder path containing the data.
    :param single_video: This is an into to choose between a single video or multiple processing.
    :param save_path: This is the path to save the processed videos to.
    :param show_video: This is a check to see if openCV show display the video or not.
    :param save_videos: This is a check to save the videos or not.
    :return:
    """
    # 0 means video folder, 1 is a single video
    if single_video == 0:
        video_number = 1
        counter_first_frame = 0
        for video_file in tqdm(glob.glob(path)):
            video_capture = cv2.VideoCapture(video_file)

            counter_first_frame = 0

            #Get the width and height of original video for the save file
            if save_videos == 1:
                width_frame = int(video_capture.get(3))
                height_frame = int(video_capture.get(4))
                save_video_path = save_path + str(video_number) + ".mp4"
                save_video_final = cv2.VideoWriter(save_video_path, cv2.VideoWriter_fourcc(*'MP42'), 30.0, (width_frame, height_frame))
                video_number += 1

            #do the processing on the video
            while video_capture.isOpened():
                ret, frame = video_capture.read()
                if ret:
                    if counter_first_frame == 0:
                        background_static_frame = frame
                        counter_first_frame += 1

                    # processed_frame = extract_background_static(frame, background_static_frame)
                    # final_contours = find_contours(processed_frame, frame, 2000)

                    if save_videos == 1:
                        save_video_final.write(final_contours)

                    if show_video == 1:
                        cv2.imshow("Processed Frame", processed_frame)
                        cv2.imshow("Original Frame", frame)
                        cv2.imshow("Contours",final_contours)

                    if cv2.waitKey(25) & 0xFF == ord('q'):
                        break
                else:
                    break

    video_capture.release()
    if save_videos == 1:
        save_video_final.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    video_folder_path = "TestingSingleVideo/*.*"
    save_path_folder = "TestingFolder/ProcessedCowStatic"
    cProfile.run('background_extract_video(video_folder_path, 0, save_path_folder, 1,0)', "output_extract_cow_static.dat")

    with open("output_time1.txt", "w") as f:
        p = pstats.Stats("output_extract_cow_static.dat", stream=f)
        p.sort_stats("time").print_stats()

    with open("output_calls1.txt", "w") as f:
        p = pstats.Stats("output_extract_cow_static.dat", stream=f)
        p.sort_stats("calls").print_stats()